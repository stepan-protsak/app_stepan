<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Weather_library
	{
		//just reference to Codeigniter instance
		public function __construct()
		{
			$this->CI =& get_instance();
		}
		

		//Returns multidimensional array wiht 5 days of weather 
		//@param $locations - locations are locations from database
		public function getWeather($locations)
		{
			foreach ($locations as $location)
			{
				//Minimum request
				//Can be city,state,country, zip/postal code, IP address, longtitude/latitude. If long/lat are 2 elements, they will be assembled. IP address is one element.
				$name = $location['name']; 
				
				//getting API key from config
				$api_key = $this->CI->config->item('api_key');

				//getting number of days to forecast from config
				$num_of_days = $this->CI->config->item('forecast_days');
				
				//getting forecast time from config
				$forecast_time = $this->CI->config->item('forecast_time');

				//setting reqest
				$options = array(
					'key' => $api_key,
					'q' => $name,
					'num_of_days' => $num_of_days,
					'tp' => $forecast_time
					);

				//forming URL for CURL
				$basicurl = $this->CI->config->item('url') . http_build_query($options);

				// Setting a cURL session
				$ch = curl_init();
				
				//Setting a cURL options	
				curl_setopt($ch, CURLOPT_URL, $basicurl);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1); 
				
				// Getting a cURL data
				$xml_response = curl_exec($ch);
				
				// Closing a curl session
				curl_close($ch);

				// Making xml string an object
				$xml = simplexml_load_string($xml_response);
				
				//filling resul array
				$result[(string) $location['name']] = array(
						array($xml->weather[0]->date,
							$xml->weather[0]->maxtempC,
							$xml->weather[0]->mintempC,
							$xml->weather[0]->hourly->windspeedKmph,
							$xml->weather[0]->hourly->pressure,
							$xml->weather[0]->hourly->humidity,
							$xml->weather[0]->hourly->visibility,
							$xml->weather[0]->hourly->precipMM),
						array(
							$xml->weather[1]->date,
							$xml->weather[1]->maxtempC,
							$xml->weather[1]->mintempC,
							$xml->weather[1]->hourly->windspeedKmph,
							$xml->weather[1]->hourly->pressure,
							$xml->weather[1]->hourly->humidity,
							$xml->weather[1]->hourly->visibility,
							$xml->weather[1]->hourly->precipMM
							),
						array(
							$xml->weather[2]->date,
							$xml->weather[2]->maxtempC,
							$xml->weather[2]->mintempC,
							$xml->weather[2]->hourly->windspeedKmph,
							$xml->weather[2]->hourly->pressure, 
							$xml->weather[2]->hourly->humidity,
							$xml->weather[2]->hourly->visibility,
							$xml->weather[2]->hourly->precipMM
							),
						array(
							$xml->weather[3]->date,
							$xml->weather[3]->maxtempC,
							$xml->weather[3]->mintempC,
							$xml->weather[3]->hourly->windspeedKmph,
							$xml->weather[3]->hourly->pressure, 
							$xml->weather[3]->hourly->humidity,
							$xml->weather[3]->hourly->visibility,
							$xml->weather[3]->hourly->precipMM
							),
						array(
							$xml->weather[4]->date,
							$xml->weather[4]->maxtempC,
							$xml->weather[4]->mintempC,
							$xml->weather[4]->hourly->windspeedKmph,
							$xml->weather[4]->hourly->pressure, 
							$xml->weather[4]->hourly->humidity,
							$xml->weather[4]->hourly->visibility,
							$xml->weather[4]->hourly->precipMM
							)
						);
			}
			return $result;
		}


	}