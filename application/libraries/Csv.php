<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Csv {

	//just reference to Codeigniter instance
	public function __construct()
	{
		$this->CI =& get_instance();
	}

	//function to validate cvs file that will be added to locations table
	//@param $file - path to file that will be validated
	public function validateLocations($file)
	{ 
		//variable to hold validation result
		$result = TRUE;

		//variable to hold validation result message or error message
		$message = 'File uploaded.';

		//variable to hold numbers of row
		$row = 0;
		
		//opening cvs file to read it
		if (($handle = fopen((string) $file, "r")) !== FALSE) 
		{
			//cycling through file and cheking what inside
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
			{
				//incrementing row number
				$row++;

				//getting number of fields in row
				$num = count($data);

				//checking number of field value
				if ($num !== 3) 
				{
					$result = FALSE;
					$message = 'Invalid number of fields. (row must have 3 fields: name, latitude, longtitude)';
				}

				//getting name value
				$name = $data[0];

				//getting latitude value
				$latitude = (float) $data[1];
				
				//getting longtitude value
				$longtitude = (float) $data[2];

				//chencking latitude values
				if ($latitude > 90 || $latitude < -90)
				{
					$message = 'Wrong latitude value in ' . $row . ' row. (latitude must be in range -90 to + 90)';
					$result = FALSE;
				}
				elseif (!preg_match('/^-?[\d*\.?\d*]+$/', $data[1]))
				{
					$message = 'Wrong latitude value in ' . $row . ' row. (latitude must contain only numbers)';
					$result = FALSE;
				}

				//chenking longtitude values
				if ($longtitude > 180 || $longtitude < -180)
				{
					$message = 'Wrong longtitude value in ' . $row . ' row. (longtitude must be in range -180 to + 180)';
					$result = FALSE;
				}
				elseif (!preg_match('/^-?[\d*\.?\d*]+$/', $data[2]))
				{
					$message = 'Wrong longtitude value in ' . $row . ' row. (longtitude must contain only numbers)';
					$result = FALSE;
				}

				//checking name values 
				if (strlen($name) == 0)
				{
					$message = 'Please specify location name in ' . $row.' row.';
					$result = FALSE;
				}
				elseif (!preg_match('/^[a-zA-Z]+$/', $name))
				{
					$message = 'Wrong name in row ' . $row . '. (name must contain only letters)';
					$result = FALSE;
				}

			}

			fclose($handle);
		}

		//chek validatio result value if false delete file ffrom server
		if ($result == FALSE)
		{
			unlink($file);
		}

		//returning message to user
		return $message;
	}
	
	//function to get number of rows in csv file
	//@param $file - path to the file
	public function getNumberOfRows($file) {
		//variable to hold numbers of row
		$rows = 0;
		
		//opening cvs file to read it
		if (($handle = fopen((string) $file, "r")) !== FALSE) 
		{
			//cycling through file and cheking what inside
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
			{
				$rows++;
			}

			fclose($handle);
		}

		return $rows;
	}

	public function getLocationsData($file, $procesed) {
		
		$this->CI->load->helper('url');
		//variable to hold numbers of row
		$rows = 0;
		
		$result = array();

		//opening cvs file to read it
		if (($handle = fopen(base_url() .'uploads/csv/'. $file, "r")) !== FALSE) 
		{
			//cycling through file and cheking what inside
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
			{
				
				if ($rows >= $procesed && $rows < $procesed + $this->CI->config->item('rows_per_run'))
				{
					$result[] = array('name' => $data[0],
									'latitude' => $data[1],
									'longtitude' => $data[2]);
				}
				$rows++;
			}

			fclose($handle);	
		}
		return $result;
	}
}