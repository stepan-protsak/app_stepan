<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crontodo extends CI_Controller {

	public function index()
	{
		//loading models
		$this->load->model('Weather_model');
		$this->load->model('Csv_model');

		//loading config
		$this->config->load('cron');

		//loading csv library
		$this->load->library('csv');

		//getting file to proces from database
		$data = $this->Csv_model->getFile();

		//cheknig if qury result in sucsesed 
		if (isset($data[0]['file_name']))
		{
		
			//getting 3 rows of data from csv file
			$csv = $this->csv->getLocationsData($data[0]['file_name'], $data[0]['procesed_rows']);
			//adding locations from csv to database
			foreach ($csv as $row) {
				$this->Weather_model->addLocation($row['name'], $row['latitude'], $row['longtitude']);
			}

			//updating file status in the table
			$this->Csv_model->updateFileStatus($data[0]['file_name']);

			$rowsPerRun = $this->config->item('rows_per_run');
			//checking if this was a last rows of data send a mail that data procesed
			if (($data[0]['procesed_rows'] + $rowsPerRun) == $data[0]['total_rows'])
			{	
				$config['protocol'] = 'mail';

				$this->load->library('email');

				$this->email->from('your@example.com', 'localshost');
				$this->email->to('someone@example.com');

				$this->email->subject('File procesed');
				$this->email->message('Informing that file '. $data[0]['file_name'] . ' is uploaded to database.');

				if (!$this->email->send($config))
				{
					show_error($this->email->print_debugger());
				}
				else
				{
					echo 'Your e-mail has been sent!';
				}

			}
		}

	}
}