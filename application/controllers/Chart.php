<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chart extends CI_Controller {

	public function index()
	{	
		//set title
		$this->layout->setTitle('Chart');

		//set meta tag
		$this->layout->setMeta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1'));
		
		// add css file
		$this->layout->addCss('standart');
	
		// add js file
		$this->layout->addJs('chartDraw');
		
		//add js file 
		$this->layout->addJs('chartEvent');

		//add google charts
		$this->layout->addExteranalJs('https://www.gstatic.com/charts/loader.js');


		//set layout
		$this->layout->page('chart', array(), Layout::LAYOUT_TYPE_NO_SIDEBARS);
	}

	public function getAjaxData($index){
		//loading weather model
		$this->load->model('Weather_model');
		
		//check if weather conditions are empty
		if ($this->Weather_model->isEmpty() == TRUE)
		{
			//getting locations from db
			$locations = $this->Weather_model->getLocations();

			//passing them to library and getting weather conditions
			$data = $this->weather_library->getWeather($locations);

			//filling the table with data 
			$this->Weather_model->updateWeather($data);
		}
		//check if updae needed
		if ($this->Weather_model->isUpdateNeeded() == TRUE)
		{
			//getting locations from db
			$locations = $this->Weather_model->getLocations();

			//passing them to library and getting weather conditions
			$data = $this->weather_library->getWeather($locations);

			//updating table data
			$this->Weather_model->updateWeather($data, TRUE);
		}


		//getting database data that wee need to the view
		$data = $this->Weather_model->getChartData($index);
		$this->output->set_content_type('application/json')
						->set_output(json_encode($data));

	}

}
