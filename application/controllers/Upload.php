<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('Csv_model');
		$this->load->library('session');
	}	

	public function index()
	{	
		//set title
		$this->layout->setTitle('CVS Upload');

		//set meta tag
		$this->layout->setMeta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1'));
		
		// add css file
		$this->layout->addCss('standart');
		
		$data['table'] = $this->Csv_model->getCsvFiles();

		// setting laytout type and content
		$this->layout->page('upload', $data, Layout::LAYOUT_TYPE_NO_SIDEBARS);

	}

	public function do_upload()
	{
		$this->load->library('csv');

		//defining upload path
		$config['upload_path'] = './uploads/csv/';
		//setting allowed file types
		$config['allowed_types'] = 'csv';

		//loading upload library with our configuration
		$this->load->library('upload', $config);

		//if upload failed display error message
		if (!$this->upload->do_upload('userfile'))
		{
			$data['message'] = $this->upload->display_errors();
			$this->session->set_flashdata('message', $data['message']);
			redirect('upload/index');
		}
		else
		{
			//getting data of uploaded file
			$data = $this->upload->data();

			//validating file content
			//if file content is wrong deleting it from server
			$data['message'] = $this->csv->validateLocations($data['full_path']);
			
			$this->session->set_flashdata('message', $data['message']);
			//checking validation status and adding file to database
			if ($data['message'] == 'File uploaded.')
			{
				$this->Csv_model->addFile($data['file_name'], $this->csv->getNumberOfRows($data['full_path']));
			}
			//loading index
			redirect('upload/index');
		}
	}

	public function delete($file)
	{
		//loading model function to delete file rom db
		$this->Csv_model->deleteCsv($file);
		//deleting file from server
		unlink('uploads/csv/'. $file);
		//loading index pages
		redirect('upload/index');
	}

}