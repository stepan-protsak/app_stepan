<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	public function index()
	{	
		//set title
		$this->layout->setTitle('About');

		//set meta tag
		$this->layout->setMeta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1'));
		
		// add css file
		$this->layout->addCss('standart');
		
		//set layout
		$this->layout->page('about');
	}
}
