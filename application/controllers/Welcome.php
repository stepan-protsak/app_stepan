<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{	
		//set title
		$this->layout->setTitle('Welcome');

		//set meta tag
		$this->layout->setMeta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1'));
		
		// add css file
		$this->layout->addCss('standart');

		//add js
		$this->layout->addJs('standart');
		
		//pass some data
		$data['lol'] = 'lol';

		// setting laytout type and content
		$this->layout->page('welcome_message', $data);
		
	}
}