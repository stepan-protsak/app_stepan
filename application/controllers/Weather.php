<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Weather extends CI_Controller {

	public function index()
	{	
		//loading weather model
		$this->load->model('Weather_model');
		
		//check if weather conditions are empty
		if ($this->Weather_model->isEmpty() == TRUE)
		{
			//getting locations from db
			$locations = $this->Weather_model->getLocations();

			//passing them to library and getting weather conditions
			$data = $this->weather_library->getWeather($locations);

			//filling the table with data 
			$this->Weather_model->updateWeather($data);
		}
		//check if updae needed
		if ($this->Weather_model->isUpdateNeeded() == TRUE)
		{
			//getting locations from db
			$locations = $this->Weather_model->getLocations();

			//passing them to library and getting weather conditions
			$data = $this->weather_library->getWeather($locations);

			//updating table data
			$this->Weather_model->updateWeather($data, TRUE);
		}

		//getting database data that wee need to the view
		$pass['weather'] = $this->Weather_model->getData();

		//set title
		$this->layout->setTitle('Weather');

		//set meta tag
		$this->layout->setMeta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1'));
		
		// add css file
		$this->layout->addCss('standart');
		
		//set layout
		$this->layout->page('weathers', $pass);

	}
}
