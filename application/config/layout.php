<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//All files that needs to be autoloaded must be in 'css' or 'js' foolders, depends on what type of file you need
//Type file in array like this:
//$config['autoload'] = array('new.css');
//Where new.css - file you need to autoload
$config['autoload'] = array('bootstrap.min.css','jquery-2.2.0.min.js');