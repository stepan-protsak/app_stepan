<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_locations extends CI_Migration {

	public function up()
	{
		$this->db->query('
			CREATE TABLE locations
			(
			id int NOT NULL AUTO_INCREMENT,
			name varchar(255) NOT NULL,
			latitude float(10),
			longitude float(10),
			PRIMARY KEY (ID)
			)
			');

		$this->db->query("
			INSERT INTO locations 
			(name, latitude, longitude)
			VALUES 
			('New York', '40.714', '-74.006'),
			('Kiev', '50.433', '30.517'),
			('London', '51.517', '-0.106'),
			('Berlin', '52.517', '13.400'),
			('Tokyo', '35.690', '139.692');
			");
	}

	public function down()
	{
		$this->db->query('DROP TABLE locations');
	}

}
