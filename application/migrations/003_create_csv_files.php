<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_csv_files extends CI_Migration {

	public function up()
	{

		$this->db->query('
			CREATE TABLE csv_files
			(
			id int NOT NULL AUTO_INCREMENT,
			file_name varchar(255) NOT NULL,
			total_rows int,
			procesed_rows int,
			PRIMARY KEY (ID)
			)
			');
	}

	public function down()
	{
		$this->db->query('DROP TABLE csv_files');
	}
}