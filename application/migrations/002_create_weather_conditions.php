<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_weather_conditions extends CI_Migration {

	public function up()
	{

		$this->db->query('
			CREATE TABLE weather_conditions
			(
			param_id int NOT NULL AUTO_INCREMENT,
			date date NOT NULL,
			location_id int NOT NULL,
			max_temperature tinyint,
			min_temperature tinyint,
			wind_speed smallint,
			pressure smallint,
			humidity tinyint,
			visibility tinyint,
			precip float(5),
			PRIMARY KEY (param_id),
			FOREIGN KEY (location_id) REFERENCES locations(id)
			)
			');
	}

	public function down()
	{
		$this->db->query('DROP TABLE weather_conditions');
	}
}