				<div class="col-sm-8 text-left" id="content">
					<table>	
						<tr>
							<th colspan="100%" id="table_header">Weather</th>
						</tr>
						<tr>
							<th></th>
							<?php  foreach ($weather as $row) {
								echo '<th>' . $row['name'] . '</th>';
							} ?>
						</tr>
						<tr>
							<td class="side_headings">Max temperatrure, °C</td>
							<?php foreach ($weather as $row) {
								echo  '<td>' . $row['max_temperature'] . '</td>';
							}?>
						</tr>
						<tr>
							<td class="side_headings">Min temperatrure, °C</td>
							<?php foreach ($weather as $row) {
								echo  '<td>' . $row['min_temperature'] . '</td>';
							}?>
						</tr>
						<tr>
							<td class="side_headings">Wind speed, km/h</td>
							<?php foreach ($weather as $row) {
								echo  '<td>' . $row['wind_speed'] . '</td>';
							}?>
						</tr>
						<tr>
							<td class="side_headings">Pressure, Pa</td>
							<?php foreach ($weather as $row) {
								echo  '<td>' . $row['pressure'] . '</td>';
							}?>
						</tr>
						<tr>
							<td class="side_headings">Humidity, %</td>
							<?php foreach ($weather as $row) {
								echo  '<td>' . $row['humidity'] . '</td>';
							}?>
						</tr>
						<tr>
							<td class="side_headings">Visibility</td>
							<?php foreach ($weather as $row) {
								echo  '<td>' . $row['visibility'] . '</td>';
							}?>
						</tr>
						<tr>
							<td class="side_headings">Precip, mm</td>
							<?php foreach ($weather as $row) {
								echo  '<td>' . $row['precip'] . '</td>';
							}?>
						</tr>
					</table>
				</div>
