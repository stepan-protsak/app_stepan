<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Weather_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}


	//Method to add locations to location table
	public function addLocation($name, $latitude, $longitude)
	{	
		//checking table fort matches with $name
		$query = $this->db->query("SELECT * FROM locations WHERE name = ".$this->db->escape($name)." LIMIT 1");
		$result = $query->result_array();
		//if there is loaction with same name then don't execute the query
		if (empty($result)){
			$this->db->query("
			INSERT INTO locations (name, latitude, longitude)
			VALUES (".$this->db->escape($name).",
			".$this->db->escape($latitude).",
			".$this->db->escape($longitude).")
			");
		} 
		else 
		{
			return TRUE;
		}
	}

	//Method to get data from the database
	public function getData()
	{

		$date = date("Y-m-d");

		$query_data = $this->db->query("
			SELECT *  FROM locations 
			JOIN weather_conditions 
			ON locations.id = weather_conditions.location_id 
			WHERE date = ".$this->db->escape($date).";
			");

		return $query_data->result_array();
	}

	//Method to get locations from locations table
	public function getLocations()
	{
		//getting location that wee need 
		$request = $this->db->query('SELECT name FROM locations');		
		return $request->result_array();	
	}

	// function to chek is weather need to be updated
	// return TRUE if need and FALSE if not
	public function isUpdateNeeded()
	{
		//getting current date
		$date = date("Y-m-d");

		//getting first row of weather_conditions table
		$check = $this->db->query('SELECT * FROM weather_conditions LIMIT 1');
		$check_result = $check->result_array();

		//getting date of first record
		foreach ($check_result as $row) {
			$table_date = $row['date'];
		}

		if ($table_date !== $date) 
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	//function to check is table weather_conditions empty
	//rerturns TRUE if yes and FALSE if no
	public function isEmpty()
	{
		//getting first row of weather_conditions table
		$check = $this->db->query('SELECT * FROM weather_conditions LIMIT 1');
		$check_result = $check->result_array();

		if (empty($check_result))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}


	// function to update weather conditions
	public function updateWeather($data, $update = FALSE)
	{
		if ($update == TRUE)
		{
			$this->db->query('TRUNCATE weather_conditions;');
		}

		$request = $this->db->query('SELECT id, name FROM locations');		
		$loc = $request->result_array();

		foreach ($loc as $row) {
			//passing location name to get weather data
			
			//number of days forecast
			$num_of_days = 5;
			
			// cycling trough result aray and adding values to the database
			for($table_row=0; $table_row < $num_of_days; $table_row++)
			{	
				$location_id = $row['id'];
				$date = (string) $data[(string)$row['name']][$table_row][0];
				$max_temperature = (int) $data[(string)$row['name']][$table_row][1];
				$min_temperature = (int) $data[(string)$row['name']][$table_row][2];
				$wind_speed = (int) $data[(string)$row['name']][$table_row][3];
				$pressure = (int) $data[(string)$row['name']][$table_row][4];
				$humidity = (int) $data[(string)$row['name']][$table_row][5];
				$visibility = (int) $data[(string)$row['name']][$table_row][6];
				$precip = (int) $data[(string)$row['name']][$table_row][7];

				$insert[$table_row] = array(
						'date' => $date,
						'location_id' => $location_id,
						'max_temperature' => $max_temperature,
						'min_temperature' => $min_temperature,
						'wind_speed' => $wind_speed,
						'pressure' => $pressure,
						'humidity' => $humidity,
						'visibility' => $visibility,
						'precip' => $precip
					);
			}

		$this->db->insert_batch('weather_conditions', $insert);
		
		}
	}


	//Method to get data for chart from the database
	public function getChartData($param)
	{
		$query_data = $this->db->query("SELECT locations.name, weather_conditions.$param FROM locations JOIN weather_conditions ON locations.id = weather_conditions.location_id");

		$data = $query_data->result_array();
		
		//array that will be returned as output
		$chart = array();
		
		//insernitg numbers of days to array
		$chart[0][0] = 'Days'; 
		for ($iterator = 1; $iterator < 6; $iterator++)
		{
			$chart[$iterator][0] = $iterator; 
		}

		//sorting array for chart
		
		// initializing rows of array
		$row = 1;

		//columns of first line must be names of cities
		$nameColumn = 0;

		//array filling with values must start from 1 index cuz 0 is day value 
		$column = 1;

		//column from query resulr 
		$dataCol = 0;
		for ($iterator = 0; $iterator < count($data); $iterator++)
		{

			// cheking if we need to start another row
			if ($column > count($data)/5)
			{
				$column = 1;
				$row++;
				$dataCol = 0;
			}

			// filling first row by city names
			if($iterator%5 == 0)
			{
				$nameColumn++;
				$chart[0][$nameColumn] = $data[$iterator]['name'];
			}

			//adding to row numbered values
			$chart[$row][$column] = (int) $data[($row -1) + $dataCol][$param];
			
			//incrementing column
			$column++;

			//incrementing column data that we need
			$dataCol += 5;
		}

		//returning sorted array
		return $chart;
		
	}
}