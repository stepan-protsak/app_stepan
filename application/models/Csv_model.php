<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Csv_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	//funtion to add file in csv_files table
	//@param $name - name of file to be added
	//@param $rows - number of rows in the file
	public function addFile($name,$rows)
	{
		$this->db->query("INSERT INTO csv_files (file_name, total_rows, procesed_rows)
						VALUES (".$this->db->escape($name).",".$this->db->escape($rows).", '0')");
	}

	//function to get first file in queue
	public function getFile()
	{
		$result = $this->db->query("SELECT * FROM csv_files WHERE total_rows != procesed_rows LIMIT 1");
		return $result->result_array();
	}

	//function to update file status 
	//@param $name - name of fileS
	public function updateFileStatus($name)
	{
		$this->db->query("UPDATE csv_files 
							SET procesed_rows = procesed_rows + 3 
							WHERE file_name = ".$this->db->escape($name)."");
	}

	//function to get listing page data 
	public function getCsvFiles()
	{
		$result = $this->db->query("SELECT * FROM csv_files");
		return $result->result_array();
	}

	//funtion to delete selected file from database
	public function deleteCsv($name)
	{
		$this->db->query("DELETE FROM csv_files WHERE file_name = ".$this->db->escape($name)."");
	}
}