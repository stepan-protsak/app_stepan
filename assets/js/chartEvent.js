$(document).ready(function() {
	//loading google charts events
	google.charts.load('current', {'packages':['line']});

	//calling function to draw default selected value
	ajaxChart($('#condition').val());

	// on change of dropdown callin ajaxChart function to rebuild the chart
	$("#condition").change(function () {	
		
		ajaxChart($('#condition').val());

	});
});