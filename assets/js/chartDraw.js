//function to draw specific chart
function chart(rows, title, subtitle) {
	google.charts.setOnLoadCallback(drawChart);	
	function drawChart() {

	var data = google.visualization.arrayToDataTable(rows);

	//defining options
	var options = {
		chart: {
		title: title,
		subtitle: subtitle
		},
		width: 900,
		height: 500
	};
	var chart = new google.charts.Line(document.getElementById('jqery'));
	chart.draw(data, options);
	}
}

// function to buil chrat on ajax request
function ajaxChart(dropVal){
	$.ajax({
		data: {dropVal: dropVal},
		url: "getAjaxData/" + String(dropVal),
		type: "post",
		success: function(data){
		
			//Setting title and subtitle of chart
			switch (dropVal) {
				case 'max_temperature':
					title = 'Maximum temperature';
					subtitle = 'In celsius';
					break;
				case 'min_temperature':
					title = 'Minimum temperature';
					subtitle = 'In celsius';
					break;
				case 'wind_speed':
					title = 'Wind speed';
					subtitle = 'In km/h';
					break;
				case 'pressure':
					title = 'Pressure';
					subtitle = 'In pascals';
					break;
				case 'humidity':
					title = 'Humidity';
					subtitle = 'In precents';
					break;
				case 'visibility':
					title = 'Visibility';
					subtitle = 'From 0 to 10 points';
					break;
				case 'precip':
					title = 'Precip';
					subtitle = 'In milimeters';
					break;
			}
			// calling function to requested value
			chart(data, title, subtitle);
		}
	});
}